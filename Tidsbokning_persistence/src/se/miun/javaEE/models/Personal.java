package se.miun.javaEE.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * @author P�r Eriksson(paer1301)
 * 
 */
@Entity
@Table(name = "personal")
public class Personal implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
    @Column
	private String id;
	
	@Column
	private String namn;
	
	@Column 
	private String password;
		
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="personal")
    private Set<Pass> shift;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNamn() {
		return namn;
	}

	public void setNamn(String namn) {
		this.namn = namn;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Pass> getPass() {
		return shift;
	}

	public void setPass(Set<Pass> pass) {
		this.shift = pass;
	}
	
	public void addShift(Pass shift){
		this.shift.add(shift);
	}

	
	public String toString(){
		return id;
	}
}
