package se.miun.javaEE.models;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * @author P�r Eriksson(paer1301)
 * 
 */
@Entity
@Table(name = "datum")
public class Datum implements Serializable{

	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
     @Column
	 private Date datum;
	
	 @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="datum")
	    private Set<Pass> pass;
	 
	 public Datum(){}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Set<Pass> getPass() {
		return pass;
	}

	public void setPass(Set<Pass> shifts) {
		this.pass = pass;
	}
	public String toString(){
		return datum.toString();
	}

}
