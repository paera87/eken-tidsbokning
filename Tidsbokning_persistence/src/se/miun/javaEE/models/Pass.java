package se.miun.javaEE.models;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * @author P�r Eriksson(paer1301)
 * 
 */
@Entity
@Table(name = "pass")
public class Pass implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	private int id;
	@Column
	private String start;
	@Column
	private String slut;
	@Column
	private double timmar;
	
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private Datum datum;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private Personal personal;
	
	public Pass(){}

	public String getInfo(){
		return datum.toString() +": " + start + "-" + slut;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Personal getPersonal() {
		return personal;
	}

	public void setPersonal(Personal personal) {
		this.personal = personal;
	}

	public String getStart() {
		return start;
	}



	public void setStart(String start) {
		this.start = start;
	}



	public String getSlut() {
		return slut;
	}



	public void setSlut(String slut) {
		this.slut = slut;
	}



	public double getTimmar() {
		return timmar;
	}



	public void setTimmar(double timmar) {
		this.timmar = timmar;
	}



	public Datum getDatum() {
		return datum;
	}



	public void setDatum(Datum datum) {
		this.datum = datum;
	}



	public String toString(){
		return start+"-"+slut;
	}

	

	
}
