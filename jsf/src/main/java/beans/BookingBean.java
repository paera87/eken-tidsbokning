package beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import Dao.DatumDAO;
import remote.StaffRemote;
import remote.BookingRemote;
import se.miun.javaEE.models.Pass;
/**
 * BookingBean
 * bean used to load and book shifts
 * 
 * @date 2016-03-04
 * @author P�r Eriksson(paer1301)
 * 
 */

@ManagedBean
@SessionScoped
public class BookingBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
    private BookingRemote bookingRemote;
	private List<DatumDAO> week;
	private int shiftToBook;
	private boolean failedToBook;
	private boolean noDatesAvailable;
	int weekNumber;
	
	public BookingBean(){}
	
	@PostConstruct
	public void init() {
		weekNumber = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
		loadShifts();
	}
	 
	public void loadShifts(){
		List<DatumDAO> tmpList = bookingRemote.getShifts(weekNumber);
		if(tmpList != null)
			week = tmpList;
		else
			setNoDatesAvailable(true);
	}
	public void changeWeek(int i){
		setFailedToBook(false);
		int tmpWeekNumber;
		if(i==1)
			tmpWeekNumber=weekNumber+1;
		else
			tmpWeekNumber=weekNumber-1;
		List<DatumDAO> tmpList =bookingRemote.getShifts(tmpWeekNumber);
		if(tmpList != null){
			week = tmpList;
			weekNumber = tmpWeekNumber;
			setNoDatesAvailable(false);
		}
		else
			setNoDatesAvailable(true);
		//refresh the page with new week
		try {
			loadShifts();
			FacesContext.getCurrentInstance().getExternalContext().redirect("tidsbokning.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void book(){
		setNoDatesAvailable(false);
		if(!bookingRemote.bookShift(shiftToBook, userId))
			setFailedToBook(true);
		else
			setFailedToBook(false);
		try {
			loadShifts();
			FacesContext.getCurrentInstance().getExternalContext().redirect("tidsbokning.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	public String userId;
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	//MONDAY
	public List<Pass> getMonday() {
		return week.get(0).getBookedShifts();
	}
	public List<Pass> getMondayFreeShifts(){
		return week.get(0).getFreeShifts();
	}
	public String getMondayDate(){
		return week.get(0).getDate().toString();
	}
	
	//TUESDAY
	public List<Pass> getTuesday() {
		return week.get(1).getBookedShifts();
	}
	public List<Pass> getTuesdayFreeShifts(){
		return week.get(1).getFreeShifts();
	}
	public String getTuesdayDate(){
		return week.get(1).getDate().toString();
	}
	
	//WEDNESDAY
	public List<Pass> getWednesday() {
		return week.get(2).getBookedShifts();
	}
	public List<Pass> getWednesdayFreeShifts(){
		return week.get(2).getFreeShifts();
	}
	public String getWednesdayDate(){
		return week.get(2).getDate().toString();
	}
	
	//THURSDAY
	public List<Pass> getThursday() {
		return week.get(3).getBookedShifts();
	}
	public List<Pass> getThursdayFreeShifts(){
		return week.get(3).getFreeShifts();
	}
	public String getThursdayDate(){
		return week.get(3).getDate().toString();
	}
	
	//FRIDAY
	public List<Pass> getFriday() {
		return week.get(4).getBookedShifts();
	}
	public List<Pass> getFridayFreeShifts(){
		return week.get(4).getFreeShifts();
	}
	public String getFridayDate(){
		return week.get(4).getDate().toString();
	}
	
	//SATURDAY
	public List<Pass> getSaturday() {
		return week.get(5).getBookedShifts();
	}
	public List<Pass> getSaturdayFreeShifts(){
		return week.get(5).getFreeShifts();
	}
	public String getSaturdayDate(){
		return week.get(5).getDate().toString();
	}
	
	//SUNDAY
	public List<Pass> getSunday() {
		return week.get(6).getBookedShifts();
	}
	public List<Pass> getSundayFreeShifts(){
		return week.get(6).getFreeShifts();
	}
	public String getSundayDate(){
		return week.get(6).getDate().toString();
	}
	
	
	
    public int getShiftToBook() {
		return shiftToBook;
	}

	public void setShiftToBook(int shiftToBook) {
		
		
		this.shiftToBook = shiftToBook;
	}
	public boolean isFailedToBook() {
		return failedToBook;
	}

	public void setFailedToBook(boolean failedToBook) {
		this.failedToBook = failedToBook;
	}

	public boolean isNoDatesAvailable() {
		return noDatesAvailable;
	}

	public void setNoDatesAvailable(boolean noDatesAvailable) {
		this.noDatesAvailable = noDatesAvailable;
	}
	
	
}
