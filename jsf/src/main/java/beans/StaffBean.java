package beans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import Dao.PassDAO;
import remote.StaffRemote;
import se.miun.javaEE.models.Pass;
/**
 * StanffBean
 * bean used to show information about staff member's booked shifts
 * 
 * @date 2016-03-04
 * @author P�r Eriksson(paer1301)
 * 
 */
@ManagedBean
@RequestScoped
public class StaffBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
    private StaffRemote staffRemote;
	
	@ManagedProperty(value="#{authenticationBean}")
	private AuthenticationBean authenticationBean;
	
	

	private PassDAO bookedShifts;
	
	@PostConstruct
	public void init() {
		bookedShifts = staffRemote.getBookedShifts(authenticationBean.getUserId());
	}
	 
	public int monthlyHours(int month) {
		return bookedShifts.getMonthlyHours(month);
	}
	public List<Pass> getMonthShift(int month){
		return bookedShifts.getMonthShift(month);
	}
	
	public AuthenticationBean getAuthenticationBean() {
		return authenticationBean;
	}

	public void setAuthenticationBean(AuthenticationBean authenticationBean) {
		this.authenticationBean = authenticationBean;
	}
	 
	
}
