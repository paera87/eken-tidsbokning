package beans;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import remote.StaffRemote;
/**
 * Authentication bean
 * bean used to log in or out
 * 
 * @date 2016-03-04
 * @author P�r Eriksson(paer1301)
 * 
 */
@ManagedBean
@SessionScoped
public class AuthenticationBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private StaffRemote loginBean;
	private String userName;
	private String userId;
	private String password;
		
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
		
	public String validate(){
		userName = loginBean.validateUser(userId, password);
		if(userName!=null)
			return "success";
		else
			return "failure";
	}
	public void logOut(){
		userName = null;
		userId = null;
		password = null;
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
