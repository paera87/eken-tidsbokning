package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.AuthenticationBean;
import beans.BookingBean;
/**
 * Filter checking if user is logged in before opening pages tidsbokning and din-sida. If user not logged in, redirect to start page.
 * @author P�r Eriksson(paer1301)
 * 
 */
public class AuthenticationFilter implements Filter {
	  
	  @Override
	  public void doFilter(ServletRequest request, ServletResponse response,
		        FilterChain chain)
		        throws IOException, ServletException {
		    System.out.println("Entered authentication filter");
		    HttpServletRequest req = (HttpServletRequest) request;
		    AuthenticationBean login = (AuthenticationBean) req.getSession().getAttribute("authenticationBean");
		    String path = req.getRequestURI().substring(req.getContextPath().length());
		    System.out.println("path:" + path);

		    if (path.contains("/tidsbokning") || path.contains("/din-sida")) {
		        if (login != null) {
		            if (login.getUserName() != null && !login.getUserName().equals("")) {
		                chain.doFilter(request, response);
		            } else {
		                HttpServletResponse res = (HttpServletResponse) response;
		                res.sendRedirect("index.xhtml");
		            }
		        } else {
		            HttpServletResponse res = (HttpServletResponse) response;
		            res.sendRedirect("index.xhtml");
		        }


		    } else {
		        chain.doFilter(request, response);
		    }
		}
	  @Override
	  public void init(FilterConfig config) throws ServletException {
	  
	  }
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	}