package local;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Dao.DatumDAO;
import Dao.PassDAO;
import remote.StaffRemote;
import se.miun.javaEE.models.Datum;
import se.miun.javaEE.models.Pass;
import se.miun.javaEE.models.Personal;
/**
 * Session Bean implementation class StaffEJB
 * To perform operations on staff
 * @date 2016-03-04
 * @author P�r Eriksson(paer1301)
 */
@Stateless(name="StaffEJB")
public class StaffEJB implements StaffRemote {
	@PersistenceContext(unitName = "ljungbacken")
	private EntityManager manager;
	/**
     * validates login by user
     * @param id - the user's unique id
     * @param pw - the password
     * @return the name of the user
     */
	@Override
	public String validateUser(String id, String pw) {
		 List<Personal> list = manager.createQuery("Select p from Personal p where p.id = :Id and p.password = :Pw")
		 .setParameter("Id",id)
		 .setParameter("Pw",pw)
		 .getResultList();
			 
		 	if (list == null || list.isEmpty())
		        return null;
		 	else
		 		return list.get(0).getNamn();
	}
	/**
     * gets all the shifts booked by the user
     * @param userId - the user's unique id
     * @return PassDAO, containing a list of shifts booked by the user
     */
	@Override
	public PassDAO getBookedShifts(String userId) {
		Personal staff = manager.find(Personal.class, userId);
		List<Pass> list = manager.createQuery("Select p from Pass p where p.personal = :staff"
				+ " ORDER BY p.datum")
			 .setParameter("staff",staff)
			 .getResultList();
		
		return new PassDAO(list);
	}

	
	
}
