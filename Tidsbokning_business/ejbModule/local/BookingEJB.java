package local;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import Dao.DatumDAO;
import remote.BookingRemote;
import se.miun.javaEE.models.Datum;
import se.miun.javaEE.models.Pass;
import se.miun.javaEE.models.Personal;

/**
 * Session Bean implementation class ShiftEJB
 * To perform operations on Users. Implements remote-interface.
 * 
 * @date 2016-03-04
 * @author P�r Eriksson(paer1301)
 * 
 */
@Stateless(name="BookingEJB")
public class BookingEJB implements BookingRemote{

	@PersistenceContext(unitName = "ljungbacken")
	private EntityManager manager;
	 
	/**
     * gets all shifts within a certain week from the database
     * @param week - the week number
     * @return a list of DatumDAO, each containing the shifts for their day
     */
	@Override
	public List<DatumDAO> getShifts(int week) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.YEAR, 2016);
		Date startDate = calendar.getTime();//first day of week
	
		calendar.clear();
		calendar.set(Calendar.WEEK_OF_YEAR, week+1);
		calendar.set(Calendar.YEAR, 2016);
		Date endDate = calendar.getTime();//first day of NEXT week
				
		List<Datum> dates = manager.createQuery("Select d FROM Datum d"
				+ " WHERE d.datum BETWEEN :startDate AND :endDate"
				+ " ORDER BY d.datum")
				.setParameter("startDate", startDate,TemporalType.DATE)
				.setParameter("endDate", endDate,TemporalType.DATE).getResultList();
		List<DatumDAO> datumDAOList = new ArrayList<>();
		for(Datum d: dates)
			datumDAOList.add(new DatumDAO(d.getDatum(),d.getPass()));
		if(datumDAOList.size()<7)
			return null;
		return datumDAOList;
		
	}

	/**
     * tries to book a shift
     * @param shiftId - the shift to book
     * @param userId - the staff member to book the shift to
     */
	@Override
	public boolean bookShift(int shiftId, String userId) {
		
		Pass shift = manager.find(Pass.class, shiftId);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		Datum datum = shift.getDatum();
		
		Personal staff = manager.find(Personal.class, userId);
		List<Pass> alreadyBooked = (List<Pass>) manager.createQuery("Select p FROM Pass p"
				+ " WHERE p.datum = :shiftDate AND"
				+ " p.personal = :staff")
				.setParameter("shiftDate", datum)
				.setParameter("staff", staff).getResultList();
		if(alreadyBooked.size()>0)
			return false;
		
		staff.addShift(shift);
		shift.setPersonal(staff);
		manager.persist(staff);
		
		return true;
	}


	
}
