package Dao;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


import se.miun.javaEE.models.Pass;


/**
 * @author P�r Eriksson(paer1301)
 */
public class DatumDAO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Date date;
	 private List<Pass> shifts;
	
	 public DatumDAO(Date date, Set<Pass> shifts){
		 this.date =date;
		 this.shifts = new ArrayList<Pass>();
		 this.shifts.addAll(shifts);
	 }

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Pass> getShifts() {
		return shifts;
	}

	public void setShifts(List<Pass> shifts) {
		this.shifts = shifts;
	}

	public List<Pass> getFreeShifts() {
		List<Pass> freeShifts = new ArrayList<>();
		
		for(Pass p : shifts){
			if(p.getPersonal() == null)
				freeShifts.add(p);
		}
		
		return freeShifts;
	}

	public List<Pass> getBookedShifts() {
		List<Pass> bookedShifts = new ArrayList<>();
		
		for(Pass p : shifts){
			if(p.getPersonal() != null)
				bookedShifts.add(p);
		}
		
		return bookedShifts;
	}
	 
	 
}
