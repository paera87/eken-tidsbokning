package Dao;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;

import se.miun.javaEE.models.Pass;
/**
 * @author P�r Eriksson(paer1301)
 */
public class PassDAO implements Serializable {

	
	private List<Pass> shifts;
	
	 public PassDAO(List<Pass> shifts){
		 this.shifts = shifts;
	 }

	public List<Pass> getShifts() {
		return shifts;
	}

	public void setShifts(List<Pass> shifts) {
		this.shifts = shifts;
	}
	

	 public int getMonthlyHours(int month) {
		 Calendar cal1 = Calendar.getInstance();
		 int hours = 0;
		 for(Pass p : shifts){
			 cal1.setTime(p.getDatum().getDatum());
			 if (cal1.get(Calendar.MONTH)== month)
				 hours+=p.getTimmar();
		 }
		 return hours;
	 }

	public List<Pass> getMonthShift(int month) {
		Calendar cal1 = Calendar.getInstance();
		 List<Pass> monthShift = new ArrayList<>();
		 for(Pass p : shifts){
			 cal1.setTime(p.getDatum().getDatum());
			 if (cal1.get(Calendar.MONTH)== month)
				 monthShift.add(p);
		 }
		 return monthShift;
		
	}
	 
}
