package remote;

import java.util.List;

import javax.ejb.Remote;

import Dao.DatumDAO;
import se.miun.javaEE.models.Pass;
/**
 * @author P�r Eriksson(paer1301)
 */
@Remote
public interface BookingRemote {
	
	public List<DatumDAO> getShifts(int week);
	
	public boolean bookShift(int shiftId, String userId);
}
