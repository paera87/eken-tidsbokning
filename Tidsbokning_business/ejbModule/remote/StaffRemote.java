package remote;

import java.util.List;

import javax.ejb.Remote;

import Dao.PassDAO;
import se.miun.javaEE.models.Pass;
/**
 * @author P�r Eriksson(paer1301)
 */
@Remote
public interface StaffRemote {

	public String validateUser(String user, String pw);
	public PassDAO getBookedShifts(String userId);
}
